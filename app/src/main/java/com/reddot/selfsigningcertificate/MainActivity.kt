package com.reddot.selfsigningcertificate

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.reddot.selfsigningcertificate.network.ApiClient
import com.reddot.selfsigningcertificate.network.ForceUpdateResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        callAPI()
    }

    private fun callAPI() {
        // ApiClient.getClient(this);
        val call: Call<ForceUpdateResponse> =
            ApiClient.getClient(this).getForceUpdateInfo("ANDROID")

        call.enqueue(object : Callback<ForceUpdateResponse> {
            override fun onFailure(call: Call<ForceUpdateResponse>, t: Throwable) {
                Log.d("TAG", "Failed")
            }

            override fun onResponse(
                call: Call<ForceUpdateResponse>,
                response: Response<ForceUpdateResponse>
            ) {
                Log.d("TAG", "SUCcess")
            }

        })
    }
}
