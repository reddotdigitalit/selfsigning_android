package com.reddot.selfsigningcertificate.network

import com.google.gson.annotations.SerializedName

data class ForceUpdateResponse (

		@SerializedName("forceUpdateVersion") val forceUpdateVersion : String,
		@SerializedName("blacklistVersion") val blacklistVersion : String,
		@SerializedName("message") val message : String
)