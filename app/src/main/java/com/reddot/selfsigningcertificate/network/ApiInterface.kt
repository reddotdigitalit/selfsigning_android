package com.reddot.selfsigningcertificate.network

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiInterface {


    @GET(HttpParams.API_FORCE_UPDATE)
    fun getForceUpdateInfo(@Query(HttpParams.PARAM_PLATFORM)platform: String): Call<ForceUpdateResponse>

}