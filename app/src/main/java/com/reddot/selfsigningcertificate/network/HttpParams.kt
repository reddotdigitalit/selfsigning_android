package com.reddot.selfsigningcertificate.network


class HttpParams {

    companion object {
        const val BASE_URL: String = "https://api.bdkepler.com/api_middleware-0.0.1-RELEASE/"
        //const val BASE_URL: String = "http://54.169.94.55:8080/api_middleware-0.0.1-TESTING/"


        const val API_FORCE_UPDATE = "check_forceupdate_version"
        const val PARAM_PLATFORM = "platform"

    }
}