package com.reddot.selfsigningcertificate.network

import android.content.Context
import com.google.gson.GsonBuilder
import okhttp3.CertificatePinner
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


object ApiClient {

    fun getClient(context: Context): ApiInterface
        {
            val gson = GsonBuilder().setLenient().create()
            val interceptor = HttpLoggingInterceptor()
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)

            val certificatePinner = CertificatePinner.Builder()
                .add(
                    "api.bdkepler.com",
                    "sha256/3x6wMfr+r+kzV4zHBzumo6D6U9pvGPdrEvoB7ILeI60="
                ).build()

            val okHttpClient = OkHttpClient.Builder()
                .certificatePinner(certificatePinner)
                .build()

            val retrofit = Retrofit.Builder()
                .baseUrl(HttpParams.BASE_URL).client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()

            return retrofit.create(ApiInterface::class.java)
        }
}